# DIGITAL REPUBLIC CODE CHALLENGE



## Executando Projeto

Para executar a API do desafio é necessário abrir a pasta "calculador-tinta.API" e executar a solution de mesmo nome no visual studio, após carregadar só é preciso clicar no botão "play" verde;

Para executar o Front-end é preciso abrir a pasta "calculador-tinta" e no terminal executar nessa pasta o comando "npm install", para instalar as dependências do projeto. Após instaladas pode ser executado o comando ng serve para rodar o projeto localmente. Para acessa-lo basta acessar "localhost:4200" no navegador

## Considerações

Embora simples, fiz o projeto considerando possíveis expansões, então algumas decisões de layout e desenho das classes foram tomadas com essa mentalidades, por exemplo:

Embora os tamanhos de portas e janelas sejam fixos na versão atual, uma evolução natural do projeto seria permitir adicionar portas e janelas de tamanhos diferentes, para isso, precisariamos apenas desbloquear os campos altura e largura no bloco de portas e janelas para os usuários alterarem.

Outra evolução possível seria poder definir qual cor de tinta seria utilizada em cada parede, fazendo assim um cálculo mais avançado separando a quantidade de latas e os tamanhos para cada cor de tinta.

Uma terceira evolução possível seria trabalhar com uma lista de salas, dessa forma os usuários poderiam adicionar todas os comôdos que deseja pintar na sua casa e juntando essa evolução com a evolução das cores de tintas o usuário poderia rapidamente calcular a quantidade e tamanho de baldes necessários para cada cor de tinta para toda sua casa.
