﻿using calculador_tinta.Business.Implementation;
using calculador_tinta.Business.Interface;
using calculador_tinta.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace calculador_tinta.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TintaController : ControllerBase
    {
        private readonly ITintaBusiness _tintaBusiness = new TintaBusiness();
       // POST api/<TintaController>
        [HttpPost("TamanhoLatas")]
        public IActionResult TamanhoLatas([FromBody] Sala sala)
        {
            var retorno = _tintaBusiness.CalcularTamanhoLatas(sala);
            return Ok(retorno);
        }
    }
}
