﻿using calculador_tinta.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace calculador_tinta.Business.Interface
{
    public interface ITintaBusiness
    {
        List<double> CalcularTamanhoLatas(Sala sala);
    }
}
