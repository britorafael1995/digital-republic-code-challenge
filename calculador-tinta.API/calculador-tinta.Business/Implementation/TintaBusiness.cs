﻿using calculador_tinta.Business.Interface;
using calculador_tinta.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace calculador_tinta.Business.Implementation
{
    public class TintaBusiness : ITintaBusiness
    {
        private readonly List<double> tamanhosLatas = new List<double>() { 0.5, 2.5, 3.6, 18 };
        public List<double> CalcularTamanhoLatas(Sala sala)
        {
            var areaParedes = sala.Paredes.Sum(p => p.Largura * p.Altura);
            var portas = sala.Paredes.SelectMany(p => p.Portas);
            var janela = sala.Paredes.SelectMany(p => p.Janelas);
            var latasNecessarias = new List<double>();

            var areaPortas = portas.Sum(p => p.Largura * p.Altura);
            var areaJanelas = janela.Sum(j => j.Largura * j.Largura);

            var areaSala = areaParedes - (areaPortas + areaJanelas);

            var qtdLitrosNecessarios = areaSala / 5;

            var maisProx = tamanhosLatas.Aggregate((x, y) => Math.Abs(x - qtdLitrosNecessarios) < Math.Abs(y - qtdLitrosNecessarios) ? x : y);

            latasNecessarias.Add(maisProx);
            while (maisProx < qtdLitrosNecessarios)
            {
                qtdLitrosNecessarios -= maisProx;

                maisProx = tamanhosLatas.Aggregate((x, y) => Math.Abs(x - qtdLitrosNecessarios) < Math.Abs(y - qtdLitrosNecessarios) ? x : y);

                latasNecessarias.Add(maisProx);
            }

            return latasNecessarias;
        }
    }
}
