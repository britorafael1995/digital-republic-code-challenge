﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace calculador_tinta.Model
{
    public class Parede
    {
        public double Altura { get; set; }
        public double Largura { get; set; }

        public List<Janela> Janelas { get; set; }
        public List<Porta> Portas { get; set; }
    }
}
