import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path: '',     pathMatch: 'full', redirectTo: 'calcular'},
  {path: 'calcular', loadChildren: () => import('./calcular-tinta/calcular-tinta.module').then(m => m.CalcularTintaModule)},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    useHash: true
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
