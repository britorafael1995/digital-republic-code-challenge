import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Sala } from '../shared/model/sala';

@Injectable({
  providedIn: 'root'
})
export class TintaService {
  private baseUrl = `${environment.baseUrl}/api/Tinta`;
  constructor(private http: HttpClient) { }

  calcularTamanhoLatas(Sala: Sala){
    return this.http.post(`${this.baseUrl}/TamanhoLatas`, Sala)
  }
}
