import { TestBed } from '@angular/core/testing';

import { TintaService } from './tinta.service';

describe('TintaService', () => {
  let service: TintaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TintaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
