import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class UtilProvider {



    constructor() { }


    validarAlteracaoTamanhoParede(parede: any) {
        const areaParede = parede.largura * parede.altura;
        const areaPortasJanelas = this.calcularAreaPortasJanelas(parede);
        if (parede.altura == 0) {
            alert("Sua parede não pode ter a altura zerada")
            return false;
        }
        else if (parede.largura == 0) {
            alert("Sua parede não pode ter a largura zerada")
            return false;
        }
        else if (areaParede < 1) {
            alert("A área da sua parede não pode ser inferior a 1 metro");
            return false;
        }
        else if (areaParede > 15) {
            alert("A área da sua parede não pode ser superior a 15 metros");
            return false;
        }
        else if (areaPortasJanelas > (areaParede / 2)) {
            alert("A área total das portas e janelas não pode ser superior a 50% da área da parede");
            return false;
        }
        else if (!this.validarDiferencaAlturaParedePorta(parede)) {
            alert("A altura da parede precisa ser 30 centimetros maior que a altura da porta");
            return false;
        }

        return true;
    }

    calcularAreaPortasJanelas(parede: any) {
        let areaPortas = 0;
        let areaJanelas = 0;
        if (parede.portas.length > 0) {
            areaPortas = parede.portas.map((p: any) => {
                return p.largura * p.altura
            }).reduce((a: any, b: any) => a + b);
        }

        if (parede.janelas.length > 0) {
            areaJanelas = parede.janelas.map((p: any) => {
                return p.largura * p.altura
            }).reduce((a: any, b: any) => a + b);
        }
        return areaPortas + areaJanelas;
    }

    validarDiferencaAlturaParedePorta(parede: any) {
        if (parede.portas.length > 0) {
            const alturaMaiorPorta = parede.portas.reduce((a: any, b: any) => { return Math.max(a.altura, b.altura) });
            const diferencaAltura = parede.altura - alturaMaiorPorta;
            if (diferencaAltura < 0.30)
                return false;
        }

        return true;
    }



}
