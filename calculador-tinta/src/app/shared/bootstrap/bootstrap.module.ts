import { NgModule } from '@angular/core';
import { ModalModule } from "ngx-bootstrap/modal";
import { defineLocale } from 'ngx-bootstrap/chronos';
import { ptBrLocale } from 'ngx-bootstrap/locale';
import { AccordionModule } from 'ngx-bootstrap/accordion';

defineLocale('pt-br', ptBrLocale)


@NgModule({
  imports: [
    ModalModule.forRoot(),
    AccordionModule.forRoot(),
  ],
  exports: [
    ModalModule,
    AccordionModule
  ]

})
export class BootstrapModule { }
