import { Parede } from "./parede";

export class Sala {
    constructor(){
        this.paredes = [new Parede()];
    }
    paredes: Parede[];
}