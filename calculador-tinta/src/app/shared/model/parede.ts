import { Janela } from "./janela";
import { Porta } from "./porta";

export class Parede {
    constructor() {
        this.altura = 1;
        this.largura = 1;
        this.janelas = [];
        this.portas = [];
    }
    altura: number;
    largura: number;
    janelas: Janela[];
    portas: Porta[];
}