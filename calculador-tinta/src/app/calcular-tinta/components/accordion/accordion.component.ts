import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UtilProvider } from 'src/app/shared/providers/util';

@Component({
  selector: 'app-accordion',
  templateUrl: './accordion.component.html',
  styleUrls: ['./accordion.component.scss']
})
export class AccordionComponent implements OnInit {
  @Input() titulo: string = "";
  @Input() item: any;
  @Input() isJanelaPorta: boolean = false;
  @Output() removerItem = new EventEmitter();
  constructor(
    private util: UtilProvider
  ) { }

  ngOnInit(): void {
  }

  removerAccordion() {
    this.removerItem.emit("");
  }

  validarAlteracoes() {
    this.util.validarAlteracaoTamanhoParede(this.item);
  }

}
