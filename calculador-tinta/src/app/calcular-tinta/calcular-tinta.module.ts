import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CalcularTintaRoutingModule } from './calcular-tinta-routing.module';
import { CalcularTintaComponent } from './calcular-tinta.component';
import { SharedModule } from '../shared/shared.module';
import { AccordionComponent } from './components/accordion/accordion.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    CalcularTintaComponent,
    AccordionComponent
  ],
  imports: [
    CommonModule,
    CalcularTintaRoutingModule,
    FormsModule,
    SharedModule
  ]
})
export class CalcularTintaModule { }
