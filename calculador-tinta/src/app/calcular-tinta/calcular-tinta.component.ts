import { Component, OnInit } from '@angular/core';
import { TintaService } from '../services/tinta.service';
import { Janela } from '../shared/model/janela';
import { Parede } from '../shared/model/parede';
import { Porta } from '../shared/model/porta';
import { Sala } from '../shared/model/sala';
import { UtilProvider } from '../shared/providers/util';

@Component({
  selector: 'app-calcular-tinta',
  templateUrl: './calcular-tinta.component.html',
  styleUrls: ['./calcular-tinta.component.scss']
})
export class CalcularTintaComponent implements OnInit {
  sala: Sala = new Sala();
  constructor(private tservice: TintaService,
    private util: UtilProvider) { }

  ngOnInit(): void {
  }

  adicionarParede() {
    const novaParede = new Parede();
    this.sala.paredes.push(novaParede);
  }

  adicionarPorta(parede: Parede) {
    if (this.util.validarAlteracaoTamanhoParede(parede)) {
      const novaPorta = new Porta();
      parede.portas.push(novaPorta);
    }

  }

  adicionarJanela(parede: Parede) {
    if (this.util.validarAlteracaoTamanhoParede(parede)) {
      const novaJanela = new Janela();
      parede.janelas.push(novaJanela);
    }
  }

  restaurarSala() {
    this.sala = new Sala();
  }

  calcularBaldes() {
    let tamanhosValidados = false;

    this.sala.paredes.forEach(parede => {
      if(this.util.validarAlteracaoTamanhoParede(parede))
        tamanhosValidados = true;
      else
        tamanhosValidados = false;
    })

    if(tamanhosValidados) {
      this.tservice.calcularTamanhoLatas(this.sala).subscribe((result) => {
        alert(this.contarLatasTamanhosIguais(result as string[]))
      })
    }
  }

  contarLatasTamanhosIguais(lista: string[]) {
    let textoResultado = "";
    const itensContados = lista.map((name) => {
      return {
        count: 1,
        name: name
      }
    }).reduce((a: any, b: any) => {
      a[b.name] = (a[b.name] || 0) + b.count
      return a
    }, {});

    Object.entries(itensContados).forEach(i => {
      console.log(i)

      const textoQtd = i[1] as number > 1 ? `${i[1]} latas de` : `${i[1]} lata de`;

      if (!textoResultado)
        textoResultado = `${textoQtd} ${i[0]}L`;
      else
        textoResultado = `${textoResultado} + ${textoQtd} ${i[0]}L`;
    });

    return textoResultado;
  }

  contadorTitulo(index: number) {
    return `${index + 1}`;
  }

  removerItem(index: number, lista: any[]) {
    lista = lista.splice(index, 1);
  }
}
