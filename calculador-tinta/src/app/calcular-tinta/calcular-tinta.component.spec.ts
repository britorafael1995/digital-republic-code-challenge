import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CalcularTintaComponent } from './calcular-tinta.component';

describe('CalcularTintaComponent', () => {
  let component: CalcularTintaComponent;
  let fixture: ComponentFixture<CalcularTintaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CalcularTintaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CalcularTintaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
