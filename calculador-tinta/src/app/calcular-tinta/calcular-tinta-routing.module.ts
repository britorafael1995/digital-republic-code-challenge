import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CalcularTintaComponent } from './calcular-tinta.component';

const routes: Routes = [
  {path: '', component: CalcularTintaComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CalcularTintaRoutingModule { }
